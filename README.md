<div align="center">

![Logo](Icons/launch.png)

### Starlight Launcher UI

### About Project

Utility to use Starlight with UI. 
When me back, I will add source and update it soon.

### Credits
 to Icon8 for original exe icon
 to RealNickk for Starlight Launcher

### Code License

It is BSD 3 Clause, read it [there](LICENSE)

</div>